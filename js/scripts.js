/* MÁSCARA DE CAMPOS */
$(document).ready(function () {
    var options = {
        onInvalid: function (val, e, f, invalid, options) {
            var error = invalid[0];
            alert($("#celular").length);
            /*if($("#celular").length < 3) {
                alert("TESTE");
            }*/
        }
    };

    $("#cpf").mask("999.999.999-99");
    $("#fixo").mask("(99) 99999-9999");
    $("#celular").mask("(99) 99999-9999", options);
});

/* EFEITO MOBILE FORMULÁRIO */
$(".efeito").hide();
$("#btn-formulario-a,#btn-formulario-b").click(function () {
    $("#form-box").show();
    $(".efeito").show();
});

$(".btn-close").click(function () {
    $("#form-box").hide();
    $(".efeito").hide();

    $('html, body').animate({
        scrollTop: $("body").offset().top
    }, 1000);
});

$("#eft").click(function () {
    $("#form-box").hide();
    $(".efeito").hide();
});

/* ENVIO DO FORMULÁRIO */
$("#aguarde").hide();
$('form').submit(function () {
    $("#submit").attr("disabled", "disabled");
    $("#aguarde").show();
    $("#inscreva").hide();
});


// JS REFLEXO
$(document).ready(function(){
    setInterval(function(){
        $(".reflexo").animate({
            left:510,
            opacity: 0.6
        }, 3200);
        setTimeout(function(){
            $(".reflexo").css({left:-520,opacity:0.5});
        },3900);
    },9000);
});


// JS REFLEXO
$(document).ready(function(){
    setInterval(function(){
        $(".reflexo2").animate({
            left:480,
            opacity: 0.6
        }, 900);
        setTimeout(function(){
            $(".reflexo2").css({left:-80,opacity:0});
        },1500);

    },3000);
});