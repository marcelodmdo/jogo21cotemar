<?php
    
    class Instituicoes_model extends CI_Model
    {
        public function getInstituicaoById($id = NULL)
        {
            if($id) {
                return $this->db
                    ->where("id_instituicao", $id)
                    ->get("instituicao")
                    ->row_array();
            } else {
                return array("response"=>"ID is required!");
            }
        }
    }