<?php
    
    class Cursos_model extends CI_Model
    {
        public function getCursos($area=null)
        {
            
            return $this->db
                ->select('titulo')
                ->where("instituicao_id", 9)
                ->where("area_id", $area)
                ->where("status", 1)

                ->order_by('titulo')
                ->get("cursos")
                ->result_array();
        
        }
    }