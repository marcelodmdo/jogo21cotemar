<?php
    
    class Areas_model extends CI_Model
    {
        public function getAreas()
        {
            
            return $this->db
                ->select('id,titulo')
                ->where("instituicao_id", 9)
                ->where("status", 1)
                ->order_by('titulo')
                ->get("areas")
                ->result_array();
        
        }
    }