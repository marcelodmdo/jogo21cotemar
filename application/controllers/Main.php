<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use FormDataJson_model as FormDataJson;
use Campanhas_model as Campanhas;
use Instituicoes_model as Instituicoes;
class Main extends CI_Controller {

	protected $root;
	private $pontuacao_ganha;
	private $pre_soma;
	private $count;
	private $finalCards;
	private $finalDesconto;
	
	/**
	 * Constructor that loads all necessary models
	 */
	public function __construct()
	{
		parent::__construct();
		
		$descontosArray = array(13=>20,18=>30,21=>50);
		$params = array(13 => array("broly" => 8),18 => array("broly"=>8),21 => array("broly"=>4));
		
		$array = array();
		foreach($params as $p=>$v){
			for($i = 1; $i<=$v['broly'];$i++){
				$array[] = $p;
			}
		}
		shuffle($array);
		
		$k = rand(0,(count($array)-1));
		
		$this->pontuacao_ganha = $array[$k];
		$this->finalDesconto = $descontosArray[$this->pontuacao_ganha];
		// $this->root = dirname(dirname(dirname(__FILE__))) . '/application/libraries/';
		// require_once $this->root . 'ac/ActiveCampaign.class.php';
	}

	public function index()
	{
		$campanha = $this->Campanhas_model->getCampanhaById(41);
		$instituicao = $this->Instituicoes_model->getInstituicaoById(9);
		$this->load->view('landing_page',compact("campanha","instituicao"));
	}

	public function geraCartas() {
		// echo "Pontuação ganha: ".$this->pontuacao_ganha;
		
		$this->count = 0;
		$cartas = [];
		while($this->pre_soma < $this->pontuacao_ganha){
			
			$carta = $this->spl();
			
			if($carta > 0) {
				$this->pre_soma += $carta;	
				$this->count++;
				echo "Carta: ".$carta."<br>";
				$cartas[] = $carta;
				echo ">>>>Pontuação atual: ".$this->pre_soma."<br>";
			}
				
			if($this->count == 8){
				break;
			}
		}
		return $cartas;
	}
	public function spl(){
		$v = rand(1,6);
		// echo "<hr>";
		echo "Carta escolhida: ". $v;
		echo "<br>";
		$preview_points = $this->pre_soma + $v;
		if($this->count < 4){
			if($preview_points > $this->pontuacao_ganha) {
				// echo "restorna zero<br>";
				return 0;
			} else {
				return $v;
			}
		} else {
			// echo "else";
			if($preview_points > $this->pontuacao_ganha) {
				// echo "restorna zero3<br>";
				return 0;
			} else if($preview_points > $this->pontuacao_ganha) {
				// echo "retorna 0";
				return 0;
			} else {
				// echo "bah";
				return $v;
			}
		}
		


		
	}
	
	public function submitAndStartGame21(){
		$postRequest = $_REQUEST;
		$rs = array();
		// print_r($postRequest);
		
		// grava os dados do lead no FormDataJson
		$this->load->database('default', TRUE);
        
		$dataToFDJ = array(
			"form_name"     =>      $postRequest["formID"],
			"instituicao"   =>      1,
		);

		$json_data = json_encode($postRequest);
		$dataToFDJ["json_data"] = $json_data;
		$formDataJS = new FormDataJson;
		$lid = $formDataJS->inserirFormData($dataToFDJ);
		
		if(is_int($lid)){
			if(!isset($rs["databaseFDJ"]))
				$rs["databaseFDJ"] = array();

			$rs["databaseFDJ"]["success"] = 1;
			$rs["databaseFDJ"]["lastid"] = $lid;
		} else {
			$rs["databaseFDJ"]["success"] = 0;
			$rs["databaseFDJ"]["message"] = "erro";
		}
		
		$c=1;
		while($this->pre_soma != $this->pontuacao_ganha){
			$this->pre_soma = 0;
			echo "<hr>";
			echo "Tentativa: ".$c."<br>";
			$c++;
			$cartas = $this->geraCartas();
		}
		var_dump($cartas);

		// grava os dados do lead no Jogo21cotemar
		
		$cartas = implode(',',$cartas);
		$dataToJg21 = array(
			"nome"     =>      			$postRequest["nome"],
			"telefone"   =>      		$postRequest["telefone"],
			"email"   =>      			$postRequest["email"],
			"cpf"   =>      			$postRequest["cpf"],
			"como_conheceu"   =>      	$postRequest["como_conheceu"],
			"regulamento"   =>      	$postRequest["regulamento"],
			"used_cards"   =>      		"[]",
			"cards"   =>      			$cartas,
			"final_pontos"   =>      	$this->pontuacao_ganha,
			"status"   =>      			0,
			"desconto"   =>      		$this->finalDesconto,
		);
		
		$Jg21 = new Jogo21cotemar_model;
		$lid = $Jg21->SaveLead($dataToJg21);
		
		if(is_int($lid)){
			if(!isset($rs["databaseJ21"]))
				$rs["databaseJ21"] = array();

			$rs["databaseJ21"]["success"] = 1;
			$rs["databaseJ21"]["lastid"] = $lid;
		} else {
			$rs["databaseJ21"]["success"] = 0;
			$rs["databaseJ21"]["message"] = "erro";
		}
		print_r($rs);

		// $postRequest['email'] = 'kdkdkdkd@dadsaf.com';
		// $this->RDStation_model->enviarFormulario($postRequest, "inscricao-abaete-sem-bolsa");
		// redirect('/obrigado', 'refresh');

		exit;
	}	
		

	public function thankYouPage()
	{
		$campanha = $this->Campanhas_model->getCampanhaById(41);
		$instituicao = $this->Instituicoes_model->getInstituicaoById(9);
		
		$this->load->view('thank_you_page',compact("campanha","instituicao"));
	}
	public function getCursos() {
		$postRequest = $_REQUEST;
		$areaID = $postRequest['area_synapse'];
		$cursosArea = $this->Cursos_model->getCursos($areaID);
		echo json_encode($cursosArea);

	}

	public function sendData() {
		$postRequest = $_REQUEST;
		
		$rs = array();
		if($postRequest["formID"] == "abaete"){
			
			$dataToDatabase = array(
				"form_name"     =>      $postRequest["formID"],
				// "hashCode"      =>      $hashString,
				"instituicao"   =>      9,
			);
							
			$json_data = json_encode($postRequest);
			// print_r($json_data);exit;
			$dataToDatabase["json_data"] = $json_data;
			
			$formDataJS = new FormDataJson;
			$lid = $formDataJS->inserirFormData($dataToDatabase);
			// print_r($lid);
			if(is_int($lid)){
				if(!isset($rs["database"]))
					$rs["database"] = array();

				$rs["database"]["success"] = 1;
				$rs["database"]["lastid"] = $lid;
			} else {
				$rs["database"]["success"] = 0;
				$rs["database"]["message"] = "erro";
			}
			
		} else { echo "false"; }

		
		$postRequest['email'] = 'kdkdkdkd@dadsaf.com';
		$this->RDStation_model->enviarFormulario($postRequest, "inscricao-abaete-sem-bolsa");
		redirect('/obrigado', 'refresh');
		
        
	}
}
