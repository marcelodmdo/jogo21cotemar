<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
  	<?= $instituicao["instituicao_tagmanager_head"] ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?= $campanha["campanha_titulo"] ?></title>
	
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
	
	<!-- Bootstrap -->
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/lpbootstrap.min.css" rel="stylesheet"> -->

    <!-- Medias -->
    <link href="<?= base_url() ?>css/media-geral.css?<?=time()?>" rel="stylesheet">
    <!--link href="<?= base_url() ?>css/media-xsss.css?<?=time()?>" rel="stylesheet" media="(min-width: 1px) and (max-width: 375px)">
    <link href="<?= base_url() ?>css/media-xss.css?<?=time()?>" rel="stylesheet" media="(min-width: 376px) and (max-width: 425px)">
    <link href="<?= base_url() ?>css/media-xs.css?<?=time()?>" rel="stylesheet" media="(min-width: 426px) and (max-width: 599px)">
    <link href="<?= base_url() ?>css/media-sm.css?<?=time()?>" rel="stylesheet" media="(min-width: 600px) and (max-width: 767px)">
    <link href="<?= base_url() ?>css/media-md.css?<?=time()?>" rel="stylesheet" media="(min-width: 768px) and (max-width: 991px)">
    <link href="<?= base_url() ?>css/media-mdd.css?<?=time()?>" rel="stylesheet" media="(min-width: 992px) and (max-width: 1199px)">
    <link href="<?= base_url() ?>css/media-lg.css?<?=time()?>" rel="stylesheet" media="(min-width: 1200px) and (max-width: 1440px)">
    <link href="<?= base_url() ?>css/media-xl.css?<?=time()?>" rel="stylesheet" media="(min-width: 1441px)"-->

    <!-- Fontawesome -->
    <link href="<?= base_url() ?>css/font-awesome.min.css" rel="stylesheet">

    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css'>
    <link rel="stylesheet" href="<?= base_url() ?>css/style.css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body id="tkp">
  	<?= $instituicao["instituicao_tagmanager_body"] ?>
    
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-12 text-center" style="margin-top: 200px">
          <img src="<?= base_url('/img/pos-com-seguranca/logo_vertical_white.png') ?>" />
          <h3>Agradecemos o seu interesse.</h3>
          <h3>Em breve entraremos em contato por e-mail ou telefone.</h3>
        </div>
      </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>

	
  </body>
</html>