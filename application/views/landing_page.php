<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?= $instituicao["instituicao_tagmanager_head"] ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?= $campanha["campanha_titulo"] ?></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">



    <!-- Medias -->
    <link href="<?= base_url() ?>css/media-geral.css?<?=time()?>" rel="stylesheet">
    <!--link href="<?= base_url() ?>css/media-xsss.css?<?=time()?>" rel="stylesheet" media="(min-width: 1px) and (max-width: 375px)">
	<link href="<?= base_url() ?>css/media-xss.css?<?=time()?>" rel="stylesheet" media="(min-width: 376px) and (max-width: 425px)">
	<link href="<?= base_url() ?>css/media-xs.css?<?=time()?>" rel="stylesheet" media="(min-width: 426px) and (max-width: 599px)">
	<link href="<?= base_url() ?>css/media-sm.css?<?=time()?>" rel="stylesheet" media="(min-width: 600px) and (max-width: 767px)">
	<link href="<?= base_url() ?>css/media-md.css?<?=time()?>" rel="stylesheet" media="(min-width: 768px) and (max-width: 991px)">
	<link href="<?= base_url() ?>css/media-mdd.css?<?=time()?>" rel="stylesheet" media="(min-width: 992px) and (max-width: 1199px)">
	<link href="<?= base_url() ?>css/media-lg.css?<?=time()?>" rel="stylesheet" media="(min-width: 1200px) and (max-width: 1440px)">
	<link href="<?= base_url() ?>css/media-xl.css?<?=time()?>" rel="stylesheet" media="(min-width: 1441px)"-->

    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"
        integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


    <link rel='stylesheet prefetch'
        href='https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css'>
    <link rel="stylesheet" href="<?= base_url() ?>css/style.css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?= $instituicao["instituicao_tagmanager_body"] ?>
    <section>
        <div class="container-fluid nopadding nomargin">
            <div class="row nopadding nomargin">
                <div class="col-md-12 nopadding nomargin">
                    <div class="box-wrap-dobra-1">
                        <div class="part-1">
                            <div class="col-md-6 offset-md-5 nopadding" style="">
                                <div class="row nopadding nomargin" style="">
                                    <div class="col-6 model-box">
                                        <img src="<?= base_url('img/inauguracao-abaete/model.png') ?>">
                                    </div>
                                    <div class="col-6 text-box"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script src="<?= base_url('/js/jquery.mask.js') ?>">
    </script>

    <script>
    $(document).ready(function() {
        var p = [
            {}
        ];
        $(document).on('click','.btn-nivel-opt',function(){
            
            $('.btn-nivel-opt').removeClass('active');
            $(this).addClass('active');
            $("#nivel_id_unimestre").val($(this).data('id'));
            $("#nivel").text($(this).text());
        });

        $(document).on('change','#area_synapse', function(){
            $("#curso_synapse option:first").text('carregando cursos...');
           
            
            dados = $(this).serializeArray();
            console.log(dados);
            url = base_url + 'form/getCursos';
            console.log(url);

            var posting = $.post(url, dados);
            posting.done(function(r) {
                
                var a;
                try {
                    //try to parse JSON
                    a = jQuery.parseJSON(r);
                    //use JSON
                } catch (error) {
                    //handle error -> visual feedback for user
                    console.log("%c========", "color:orange");
                    console.log(error)
                    console.log("%c========", "color:orange");
                    a = r;
                }
                console.log(a);
                $("#curso_synapse option").not('option:first').remove();
                $.each(a,function(i,curso){
                    option = '<option value='+curso.titulo+'>'+curso.titulo+'</option>';
                    $("#curso_synapse").append(option);
                });
                $("#curso_synapse option:first").text('SELECIONE UM CURSO');
                // btnSubmit.text('Enviar');
                // btnSubmit.remove('i');
                // btnSubmit.removeAttr('disabled');
                // console.log(a.database.success);
                // if (a.database.success) {
                //     window.location.replace(base_url + 'obrigado');

                //     // alert("OK!");
                // } else {
                //     alert("Error!");
                // }



            });
        });
					var options = {
						onInvalid: function (val, e, f, invalid, options) {
								var error = invalid[0];
								alert($("#celular").length);
								/*if($("#celular").length < 3) {
										alert("TESTE");
								}*/
						}
				};

				$("#celular").mask("(99) 99999-9999", options);


        var base_url = "<?= base_url('/') ?>";
        // $('#formaPagamento').val('credito');
        btnSubmit = $('button#btn-submit');
        $('#lpForm').submit(function(e) {
            e.preventDefault();

            $("#inscreva").text('carregando...');
            $("#aguarde").html('<i class="fa fa-spinner fa-spin fa-fw"></i> ');

            btnSubmit.attr('disabled', 'true');
            dados = $(this).serializeArray();

            url = base_url + 'form/post';
            console.log(url);

            var posting = $.post(url, dados);
            posting.done(function(r) {
                var a;
                try {
                    //try to parse JSON
                    a = jQuery.parseJSON(r);
                    //use JSON
                } catch (error) {
                    //handle error -> visual feedback for user
                    console.log("%c========", "color:orange");
                    console.log(error)
                    console.log("%c========", "color:orange");
                    a = r;
                }
                console.log(a);
                btnSubmit.text('Enviar');
                btnSubmit.remove('i');
                btnSubmit.removeAttr('disabled');
                console.log(a.database.success);
                if (a.database.success) {
                    window.location.replace(base_url + 'obrigado');

                    // alert("OK!");
                } else {
                    alert("Error!");
                }



            });
            return false;
        });
    });
    </script>
</body>

</html>